djview4shapes is a fork of djview4
(http://djvu.sourceforge.net/djview4.html).  Its purpose is to display
the content of common shapes dictionaries contained in DjVu
documents.

The idea to display the common shapes of a document (to facilitate
e.g. transcription and character inventory extraction) and other
suggestions were formulated by Janusz S. Bień. 

English user documentation is provided in the online help, the program
is described in Polish in "Repertuar znaków piśmiennych — problemy i
perspektywy" (https://www.researchgate.net/publication/340342236).

The program was developed by Michał Rudolf using some code of Grzegorz
Chimosz.  The work was supported by the Ministry of Science and Higher
Education's grant no. N N519 38403 (cf.
https://github.com/jsbien/ndt/wiki).

In 2020 a small but important contribution has been made by Alexander
Trufanov.

The current home of the program is
https://github.com/jsbien/djview4shapes/ and new issues should be
reported there. This repository is kept because of the open issues,
but is no longer maintained.

![djview4shapes: a screenshot](screenshots/djview4shapes_Zaborowski.png?raw=true "Index of abbreviations")
